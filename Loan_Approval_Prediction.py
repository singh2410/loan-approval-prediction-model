#!/usr/bin/env python
# coding: utf-8

# # Loan Approval Prediction using ML
# #By-Aarush Kumar
# #Dated: June 18,2021

# ## This project will predict whether a customer will get the loan from bank or not.
# ### Following Factors are required:
# 1-Gender
# 2-Education
# 3-Marrital status
# 4-Loand Amount
# 5-Credit History
# 6-Account Balance
# 7-Property Area
# 8-Credit History
# 9-Dependants
# 10-Self Employment Status

# In[294]:


import pandas as pd
import numpy as np
import sklearn


# In[295]:


train=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Loan Approval Prediction/train.csv')
train.Loan_Status=train.Loan_Status.map({'Y':1,'N':0})


# In[296]:


train


# In[297]:


train.isnull().sum()


# ## Preprocessing Data

# In[298]:


Loan_status=train.Loan_Status
train.drop('Loan_Status',axis=1,inplace=True)
test=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Loan Approval Prediction/test.csv')
Loan_ID=test.Loan_ID
data=train.append(test)
data.head()


# In[299]:


data.shape


# In[300]:


data.size


# In[301]:


data.describe()


# In[302]:


data.isnull().sum()


# In[303]:


import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
corrmat=data.corr()
f,ax=plt.subplots(figsize=(10,10))
sns.heatmap(corrmat,vmax=.8,square=True)


# ## Label ENcoding

# In[304]:


## Label encoding for gender
data.Gender=data.Gender.map({'Male':1,'Female':0})
data.Gender.value_counts()


# In[305]:


## Let's see correlations
corrmat=data.corr()
f,ax=plt.subplots(figsize=(10,10))
sns.heatmap(corrmat,vmax=.8,square=True)


# In[306]:


## Labelling 0 & 1 for Marrital status
data.Married=data.Married.map({'Yes':1,'No':0})


# In[307]:


data.Married.value_counts()


# In[308]:


## Labelling 0 & 1 for Dependents
data.Dependents=data.Dependents.map({'0':0,'1':1,'2':2,'3+':3})


# In[309]:


data.Dependents.value_counts()


# In[310]:


## Correlations
corrmat=data.corr()
f,ax=plt.subplots(figsize=(10,10))
sns.heatmap(corrmat,vmax=.8,square=True)


# In[311]:


## Labelling 0 & 1 for Education Status
data.Education=data.Education.map({'Graduate':1,'Not Graduate':0})
data.Education.value_counts()


# In[312]:


## Labelling 0 & 1 for Employment status
data.Self_Employed=data.Self_Employed.map({'Yes':1,'No':0})


# In[313]:


data.Self_Employed.value_counts()


# In[314]:


data.Property_Area.value_counts()


# In[315]:


## Labelling 0 & 1 for Property area
data.Property_Area=data.Property_Area.map({'Urban':2,'Rural':0,'Semiurban':1})
data.Property_Area.value_counts()


# In[316]:


corrmat=data.corr()
f,ax=plt.subplots(figsize=(10,10))
sns.heatmap(corrmat,vmax=.8,square=True)


# In[317]:


data.head()


# In[318]:


data.Credit_History.size


# ### Filling missing values

# In[319]:


data.Credit_History.fillna(np.random.randint(0,2),inplace=True)


# In[320]:


data.isnull().sum()


# In[321]:


data.Married.fillna(np.random.randint(0,2),inplace=True)


# In[322]:


data.isnull().sum()


# In[323]:


## Filling with median
data.LoanAmount.fillna(data.LoanAmount.median(),inplace=True)
## Filling with mean
data.Loan_Amount_Term.fillna(data.Loan_Amount_Term.mean(),inplace=True)


# In[324]:


data.isnull().sum()


# In[325]:


data.Gender.value_counts()


# In[326]:


## Filling Gender with random number between 0-2
from random import randint 
data.Gender.fillna(np.random.randint(0,2),inplace=True)


# In[327]:


data.Gender.value_counts()


# In[328]:


## Filling Dependents with median
data.Dependents.fillna(data.Dependents.median(),inplace=True)
data.isnull().sum()


# In[329]:


corrmat=data.corr()
f,ax=plt.subplots(figsize=(10,10))
sns.heatmap(corrmat,vmax=.8,square=True)


# In[330]:


data.Self_Employed.fillna(np.random.randint(0,2),inplace=True)
data.isnull().sum()


# In[331]:


data.head()


# In[332]:


## Dropping Loan ID from data, it's not useful
data.drop('Loan_ID',inplace=True,axis=1)


# In[333]:


data.isnull().sum()


# In[334]:


data.head()


# ## Splitting Data

# In[335]:


train_X=data.iloc[:614,] ## all the data in X (Train set)
train_y=Loan_status 


# In[336]:


from sklearn.model_selection import train_test_split
train_X,test_X,train_y,test_y=train_test_split(train_X,train_y,random_state=0)


# In[337]:


train_X.head()


# In[338]:


test_X.head()


# ## Using various ML Models

# In[339]:


from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier


# ### Filling the models

# In[340]:


models=[]
models.append(("Logistic Regression",LogisticRegression()))
models.append(("Decision Tree",DecisionTreeClassifier()))
models.append(("Linear Discriminant Analysis",LinearDiscriminantAnalysis()))
models.append(("Random Forest",RandomForestClassifier()))
models.append(("Support Vector Classifier",SVC()))
models.append(("K- Neirest Neighbour",KNeighborsClassifier()))
models.append(("Naive Bayes",GaussianNB()))


# In[341]:


scoring='accuracy'


# In[342]:


from sklearn.model_selection import KFold 
from sklearn.model_selection import cross_val_score
result=[]
names=[]


# In[343]:


for name,model in models:
    kfold=KFold(n_splits=10,random_state=0,shuffle=True)
    cv_result=cross_val_score(model,train_X,train_y,cv=kfold,scoring=scoring)
    result.append(cv_result)
    names.append(name)
    print(model)
    print("%s %f" % (name,cv_result.mean()))


# In[344]:


from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report


# In[345]:


LR=LogisticRegression()
LR.fit(train_X,train_y)
pred=LR.predict(test_X)
print("Model Accuracy:- ",accuracy_score(test_y,pred))
print(confusion_matrix(test_y,pred))
print(classification_report(test_y,pred))


# In[346]:


print(pred)


# In[347]:


X_test=data.iloc[614:,] 


# In[348]:


X_test.head()


# In[349]:


prediction = LR.predict(X_test)
print(prediction)


# In[350]:


t = LR.predict([[0.0,	0.0,	0.0,	1,	0.0,	1811,	1666.0,	54.0,	360.0,	1.0,	2]])


# In[351]:


print(t)


# ## Saving the model

# In[352]:


import pickle
file = r'/home/aarush100616/Downloads/Projects/Loan Approval Prediction/ML_Model.pkl'


# In[353]:


from sklearn.svm import SVC
svc = SVC()
with open(file, 'wb') as f:
    pickle.dump(svc, f)


# In[354]:


with open(file, 'rb') as f:
    k = pickle.load(f)

